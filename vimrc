set shell=/bin/bash
set nocompatible
filetype off
set rtp+=~/.vim/bundle/vundle.vim/
call vundle#rc()

Bundle 'ervandew/supertab'
Bundle '3xian/snipmate.vim'
Bundle 'scrooloose/nerdtree'
Bundle 'scrooloose/syntastic'
Bundle 'yosssi/vim-gold'
Bundle 'pangloss/vim-javascript'
Bundle 'davidhalter/jedi-vim'
Bundle 'hynek/vim-python-pep8-indent'

Bundle 'molokai'
Bundle 'vim-scripts/taglist.vim'
Plugin 'posva/vim-vue'
""Bundle 'python-rope/ropevim'
"cd ~/.vim/bundle
"git clone https://github.com/python-rope/ropevim.git

filetype plugin indent on
syntax enable

set tags=~/tags

set ffs=unix,dos,mac
set et
set shiftwidth=2
set tabstop=2
set softtabstop=2
set textwidth=0
set wrap
set nu
set fileencodings=utf-8,GBK,big5,gb2312,cp936,gb18030
set encoding=utf-8
set hlsearch
set cindent
set noru
set smarttab
set lazyredraw
set backspace=indent,eol,start
set nowb
set statusline=[%{strftime('%H:%M:%S',getftime(expand('%')))}]\ %F
set laststatus=2
"set pastetoggle=<C-p>
"set pastetoggle=<C-o>
set showmode
"set fdm=marker
set fdm=indent
set foldlevel=99

set list
set listchars=tab:\|\ ,trail:~,nbsp:_
"set iskeyword-=_

au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
let g:syntastic_mode_map = {'mode': 'passive'}
let g:syntastic_cpp_compiler_options = '-std=c++0x'
let g:syntastic_python_pylint_args="-d C0103,C0111,R0903,W0141"
nmap <leader>s :SyntasticCheck<cr>

"theme
set t_Co=256
colorscheme molokai

"shortkeys

inoremap ( ()<LEFT>
inoremap [ []<LEFT>
inoremap { {}<LEFT>
inoremap ' ''<LEFT>
inoremap " ""<LEFT>
noremap <space> za
vnoremap <space> za

vnoremap <leader>c :w !pbcopy<cr>
nmap <leader>w :w<cr>
nmap <leader>q :q<cr>
nmap <leader>a $a
inoremap <leader>l <Esc>:!ls<CR>
map <leader>2 :NERDTreeToggle<CR>
let Tlist_Inc_Winwidth = 0
map <leader>3 :Tlist<CR>
map <leader>4 :wq<CR>
map <leader>9 :call SetMouse()<CR>



fu! SetMouse()
  if &mouse == 'a'
    set mouse=
  else
    set mouse=a
  endif
endfunction

" Add the virtualenv's site-packages to vim path
py << EOF
import os.path
import sys
import vim
if 'VIRTUAL_ENV' in os.environ:
    project_base_dir = os.environ['VIRTUAL_ENV']
    sys.path.insert(0, project_base_dir)
    activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
    execfile(activate_this, dict(__file__=activate_this))
EOF

"cheetsheet
"git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
" :BundleList          - list configured bundles
" :BundleInstall(!)    - install(update) bundles
" :BundleSearch(!) foo - search(or refresh cache first) for foo
" :BundleClean(!)      - confirm(or auto-approve) removal of unused bundles
"
" see :h vundle for more details or wiki for FAQ

"opening and closing folds
"za or <space>
"
"goto def of local var
"gd
"
"<c-]> goto def
"<c-o> goback
"
"<leader>g Go to definition
"<leader>d Go to original definition

"search for pydoc
"Shift+k
